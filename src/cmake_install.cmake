# Install script for directory: /Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/jrtplib3" TYPE FILE FILES
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpapppacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpbyepacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpcompoundpacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpcompoundpacketbuilder.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcppacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcppacketbuilder.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcprrpacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpscheduler.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpsdesinfo.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpsdespacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpsrpacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtcpunknownpacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpaddress.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpcollisionlist.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpconfig.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpdebug.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpdefines.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtperrors.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtphashtable.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpinternalsourcedata.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpipv4address.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpipv4destination.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpipv6address.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpipv6destination.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpkeyhashtable.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtplibraryversion.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpmemorymanager.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpmemoryobject.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtppacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtppacketbuilder.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtppollthread.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtprandom.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtprandomrand48.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtprandomrands.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtprandomurandom.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtprawpacket.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpsession.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpsessionparams.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpsessionsources.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpsourcedata.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpsources.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpstructs.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtptimeutilities.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtptransmitter.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtptypes_win.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtptypes.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpudpv4transmitter.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpudpv6transmitter.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpbyteaddress.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/rtpexternaltransmitter.h"
    "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/extratransmitters/rtpfaketransmitter.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/usr/local/lib/libjrtp.a")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE STATIC_LIBRARY FILES "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/Debug/libjrtp.a")
    if(EXISTS "$ENV{DESTDIR}/usr/local/lib/libjrtp.a" AND
       NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/libjrtp.a")
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib" "$ENV{DESTDIR}/usr/local/lib/libjrtp.a")
    endif()
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/usr/local/lib/libjrtp.a")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE STATIC_LIBRARY FILES "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/Release/libjrtp.a")
    if(EXISTS "$ENV{DESTDIR}/usr/local/lib/libjrtp.a" AND
       NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/libjrtp.a")
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib" "$ENV{DESTDIR}/usr/local/lib/libjrtp.a")
    endif()
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/usr/local/lib/libjrtp.a")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE STATIC_LIBRARY FILES "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/MinSizeRel/libjrtp.a")
    if(EXISTS "$ENV{DESTDIR}/usr/local/lib/libjrtp.a" AND
       NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/libjrtp.a")
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib" "$ENV{DESTDIR}/usr/local/lib/libjrtp.a")
    endif()
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/usr/local/lib/libjrtp.a")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE STATIC_LIBRARY FILES "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/RelWithDebInfo/libjrtp.a")
    if(EXISTS "$ENV{DESTDIR}/usr/local/lib/libjrtp.a" AND
       NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/libjrtp.a")
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib" "$ENV{DESTDIR}/usr/local/lib/libjrtp.a")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/usr/local/lib/libjrtp.3.9.1.dylib;/usr/local/lib/libjrtp.dylib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE SHARED_LIBRARY FILES
      "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/Debug/libjrtp.3.9.1.dylib"
      "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/Debug/libjrtp.dylib"
      )
    foreach(file
        "$ENV{DESTDIR}/usr/local/lib/libjrtp.3.9.1.dylib"
        "$ENV{DESTDIR}/usr/local/lib/libjrtp.dylib"
        )
      if(EXISTS "${file}" AND
         NOT IS_SYMLINK "${file}")
        execute_process(COMMAND "/usr/bin/install_name_tool"
          -id "libjrtp.3.9.1.dylib"
          "${file}")
      endif()
    endforeach()
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/usr/local/lib/libjrtp.3.9.1.dylib;/usr/local/lib/libjrtp.dylib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE SHARED_LIBRARY FILES
      "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/Release/libjrtp.3.9.1.dylib"
      "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/Release/libjrtp.dylib"
      )
    foreach(file
        "$ENV{DESTDIR}/usr/local/lib/libjrtp.3.9.1.dylib"
        "$ENV{DESTDIR}/usr/local/lib/libjrtp.dylib"
        )
      if(EXISTS "${file}" AND
         NOT IS_SYMLINK "${file}")
        execute_process(COMMAND "/usr/bin/install_name_tool"
          -id "libjrtp.3.9.1.dylib"
          "${file}")
      endif()
    endforeach()
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/usr/local/lib/libjrtp.3.9.1.dylib;/usr/local/lib/libjrtp.dylib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE SHARED_LIBRARY FILES
      "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/MinSizeRel/libjrtp.3.9.1.dylib"
      "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/MinSizeRel/libjrtp.dylib"
      )
    foreach(file
        "$ENV{DESTDIR}/usr/local/lib/libjrtp.3.9.1.dylib"
        "$ENV{DESTDIR}/usr/local/lib/libjrtp.dylib"
        )
      if(EXISTS "${file}" AND
         NOT IS_SYMLINK "${file}")
        execute_process(COMMAND "/usr/bin/install_name_tool"
          -id "libjrtp.3.9.1.dylib"
          "${file}")
      endif()
    endforeach()
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "/usr/local/lib/libjrtp.3.9.1.dylib;/usr/local/lib/libjrtp.dylib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
        message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE SHARED_LIBRARY FILES
      "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/RelWithDebInfo/libjrtp.3.9.1.dylib"
      "/Users/adamgessel/dev/crowdmics-ios/Modules/CMAudioEngine/shared/jrtplib-3.9.1/src/RelWithDebInfo/libjrtp.dylib"
      )
    foreach(file
        "$ENV{DESTDIR}/usr/local/lib/libjrtp.3.9.1.dylib"
        "$ENV{DESTDIR}/usr/local/lib/libjrtp.dylib"
        )
      if(EXISTS "${file}" AND
         NOT IS_SYMLINK "${file}")
        execute_process(COMMAND "/usr/bin/install_name_tool"
          -id "libjrtp.3.9.1.dylib"
          "${file}")
      endif()
    endforeach()
  endif()
endif()

